#include <stdafx.h>

#include "button.h"

const GLint WIDTH = 800, HEIGHT = 600;

void setupWindow();
void clearWindow();

int main()
{
    if ( glfwInit() == GLFW_FALSE)
    {
        std::cout << "Failed to init GLFW" << std::endl;
        return EXIT_FAILURE;
    }

    setupWindow();

    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Slots machine", nullptr, nullptr);

    if(window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return EXIT_FAILURE;
    }

    int screenWidth, screenHeight;

    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    glfwMakeContextCurrent(window);

    if ( glewInit() != GLEW_OK)
    {
        std::cout << "Failedt to init GLEW" << std::endl;
        return EXIT_FAILURE;
    }

    glViewport(0, 0, screenWidth, screenHeight);

    Button button1(-0.875f, 0.5f, 0.5f, 1.0f);
    button1.bindVO();

    Button button2(-0.25f, 0.5f, 0.5f, 1.0f);
    button2.bindVO();

    Button button3(0.375f, 0.5f, 0.5f, 1.0f);
    button3.bindVO();

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();

        clearWindow();

        button1.render();
        button2.render();
        button3.render();

        glfwSwapBuffers(window);
    }

    button1.unbindVO();
    button2.unbindVO();
    button3.unbindVO();

    glfwTerminate();

    return EXIT_SUCCESS;
}

void setupWindow()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}

void clearWindow()
{
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}
