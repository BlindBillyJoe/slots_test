#include <stdafx.h>

#include "button.h"
#include "shader.h"

const float offsetX = 0.0625f;
const float width = 0.25f;

Button::Button(float x, float y, float width, float height)
    : Actor()
{
    shader = new Shader("E:/Projects/Slots_Test/src/shaders/vButtonShader", "E:/Projects/Slots_Test/src/shaders/fButtonShader");

    vertices[0] = x + width;
    vertices[1] = y;
    vertices[2] = 0.0f;
    vertices[3] = 0.0f;
    vertices[4] = 1.0f;

    vertices[5] = vertices[0];
    vertices[6] = y - height;
    vertices[7] = 0.0f;
    vertices[8] = 0.0f;
    vertices[9] = 0.0f;

    vertices[10] = x;
    vertices[11] = vertices[6];
    vertices[12] = 0.0f;
    vertices[13] = 1.0f;
    vertices[14] = 0.0f;

    vertices[15] = vertices[10];
    vertices[16] = vertices[1];
    vertices[17] = 0.0f;
    vertices[18] = 1.0f;
    vertices[19] = 1.0f;

    indices.push_back(glm::ivec3(0, 1, 3));
    indices.push_back(glm::ivec3(1, 2, 3));
}

Button::~Button()
{
    delete shader;
}

void Button::render()
{
    shader->use();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glUniform1i(glGetUniformLocation(shader->Program, "ourTexture1"), 0);

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}

void Button::bindVO()
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    glGenBuffers(1, &TBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) , vertices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(glm::ivec3) * indices.size(), &indices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_TEXTURE_BUFFER, TBO);
    glBufferData(GL_TEXTURE_BUFFER, sizeof(glm::vec2) * textures.size(), &textures[0], GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    // Load and create a texture

    // ====================
    // Texture 1
    // ====================
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1); // All upcoming GL_TEXTURE_2D operations now have effect on our texture object
    // Set our texture parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// Set texture wrapping to GL_REPEAT
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // Set texture filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    // Load, create texture and generate mipmaps
    int width = 0;
    int height = 0;
    unsigned char* image = SOIL_load_image("E:/Projects/Slots_Test/res/apple.png", &width, &height, 0, SOIL_LOAD_RGB);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0); // Unbind texture when done, so we won't accidentily mess up our texture.
}

void Button::unbindVO()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);
}

void Button::setPos(const glm::vec3& vec)
{
    (void)(vec);
}

void Button::setSize(const glm::vec2& vec)
{
    (void)(vec);
}
