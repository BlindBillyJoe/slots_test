#ifndef BUTTON_H
#define BUTTON_H

#include <vector>

#include "actor.h"
class Shader;

class Button : public Actor
{
public:
    Button(float x, float y, float width, float height);
    ~Button();

    void render() override;
    void bindVO() override;
    void unbindVO() override;

    void setPos(const glm::vec3& vec) override;
    void setSize(const glm::vec2& vec) override;

private:
    unsigned int VBO, VAO, EBO, TBO;

    float vertices [20];
//    std::vector<glm::vec3> vertices;
    std::vector<glm::ivec3> indices;

    std::vector<glm::vec2> textures;

    unsigned int texture1;

    Shader* shader;
};

#endif //BUTTON_H
