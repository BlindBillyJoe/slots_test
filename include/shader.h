#ifndef SHADER_H
#define SHADER_H

class Shader
{
public:
    // Идентификатор программы
    unsigned int Program;
    // Конструктор считывает и собирает шейдер
    Shader(const char* vertexPath, const char* fragmentPath);
    // Использование программы
    void use();
};

#endif
