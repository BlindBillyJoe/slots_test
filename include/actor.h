#ifndef ACTOR_H
#define ACTOR_H

#include "glm/glm.hpp"

class Actor
{
public:
    Actor();
    virtual ~Actor();

    virtual void render() = 0;
    virtual void bindVO();
    virtual void unbindVO();

    virtual void setPos(const glm::vec3& vec); // float координаты верхней левой точки
    virtual void setSize(const glm::vec2& vec); //float ширина и высота
};

#endif //ACTOR_H
